//Watcher of Time and Space by Samar Productions 2019
//Code by Golara
//Graphics by Isildur
//Music by Jammer


//I feel like the effect is so weak I have to describe
//what it is lol...

//So, this is a blocky big scroller, but it goes through
//some weird shape and changes a color when it happens. 
//How is it made ? The normal chars on left and right
//are always red. The middle is always white and a simple
//blue sprite is put under the chars for a background.

//Then on the sides there are some sprites with non blocky shapes
//Blue under the charset for a background color
//(and also as a mask, you'll see later in the code)
//and white sprites with the same shape and the charset
//copied onto it covering the chars that are flickering between
//white and red because of the $d016 scroller


//This is another 12 or so hours coding session, so excuse some weird
//names and weird code, this has been not refactored in any way except for
//adding comments (I almost never leave any comments just on my own..)
//and moving the ZP stuff to the top of the file (that's why there's almost 2
//identical labels heheh)

.const scrollypos = 2

* = $02 "ZP STUFF" virtual
zpscrollptr:        .fill 02, $00
zpcolorcycleptr:    .fill 02, $00
zptmp_1:            .fill 01, $00
zptmp1:             .fill 01, $00


.var muzak = LoadSid("HereEverywhere7000.sid")

:BasicUpstart2(entry)
entry:
        sei
        lda #$35
        sta $01
        lda #$7f
        sta $dc0d
        sta $dd0d
        lda $dc0d
        lda $dd0d

        ldx #$ff
        txs

        lda #<colorcycle1
        sta zpcolorcycleptr
        lda #>colorcycle1
        sta zpcolorcycleptr+1

        lda #<irq_top_1
        sta $fffe
        lda #>irq_top_1
        sta $ffff
        lda #1
        sta $d012

        lda #$00
        jsr muzak.play

        lda #$18
        sta $d018

        lda #$00
        sta $d021
        sta $d020

//charlines 2-9 to red
        lda #$02
        ldx #39
!:
        .for (var i = scrollypos; i < scrollypos+7; i++)
        {
            sta $d800 +i*40,x
        }
        dex
        bpl !-
//make the middle chars white
        lda #$01
        ldx #11
!:
        .for (var i = 2; i < 7+2; i++)
        {
            sta $d800 + i*40 + 13,x
        }
        dex
        bpl !-
//copy rest of the colors of the bitmap
        ldx #39
!:
        .for (var i = 0; i < 2; i++)
        {
            lda colorram+i*40,x
            sta $d800 + i*40,x
        }
        .for (var i = 0; i < 25-9; i++)
        {
            lda colorram+(2+i)*40,x
            sta $d800 + (9+i)*40,x
        }
        dex
        bpl !-

        lda #<text
        sta zpscrollptr
        lda #>text
        sta zpscrollptr+1

        lda #$1b
        sta $d011

        lda #$01
        sta $d01a
        sta $d019
        cli
endloop:
        jmp endloop
//setup sprites for SAMAR logo
irq:
{
        pha
        lda #$fc
        sta $d015

        lda #$f0
        sta $d01b

        lda #$c0
        sta $d017
        sta $d01d

        lda #$00
        sta $d010
        sta $d01c
        
        lda #$0e
        sta $d027+4
        sta $d027+5
        sta $d027+6
        sta $d027+7
        lda #$01
        sta $d027+2
        sta $d027+3

        lda #24+88
        sta $d000 + 4*2
        sta $d000 + 2*2

        lda #24+200
        sta $d000 + 5*2
        sta $d000 + 3*2

        lda #24+112
        sta $d000 + 6*2

        lda #24+152
        sta $d000 + 7*2



        lda #66
        sta $d001 + 2*7
        sta $d001 + 2*6
        sta $d001 + 2*5
        sta $d001 + 2*4
        sta $d001 + 2*3
        sta $d001 + 2*2

        lda #sprite1/$40
        sta screen_pointer+$3f8+7
        sta screen_pointer+$3f8+6

        lda #sprite_sidesl/$40
        sta screen_pointer+$3f8+4
        lda #sprite_sidesr/$40
        sta screen_pointer+$3f8+5
   
        lda #sprite_letterl/$40
        sta screen_pointer+$3f8+2
        lda #sprite_letterr/$40
        sta screen_pointer+$3f8+3

        lda #<irq_go_scroll
        sta $fffe
        lda #>irq_go_scroll
        sta $ffff
        lda #66
        sta $d012
        inc $d019
        cli

        pla
        rti
}
//change the $d016 to scroll the text (irq happens where the scroll starts)
irq_go_scroll:
{
        pha
        lda #<irq2
        sta $fffe
        lda #>irq2
        sta $ffff
        lda #66+18
        sta $d012

        lda #$02
        sta $dd00
        lda #$9a
        sta $d018
        lda #$1b
        sta $d011
        lda scrll:#00
        sta $d016

        

        pla
        inc $d019
        rti
}
//multiplex sprites for the scrolltext
irq2:
{
        lda #66+21
        sta $d001 + 2*7
        sta $d001 + 2*6
        sta $d001 + 2*5
        sta $d001 + 2*4
        sta $d001 + 2*3
        sta $d001 + 2*2

        ldx #sprite_letterl/$40 + $1
        ldy #sprite_letterr/$40 + $1

!:      cmp $d012
        bne !-

        stx screen_pointer+$3f8+2
        sty screen_pointer+$3f8+3

        lda #sprite_sidesl/$40 + $1
        sta screen_pointer+$3f8+4
        lda #sprite_sidesr/$40 + $1
        sta screen_pointer+$3f8+5

        

        lda #<irq3
        sta $fffe
        lda #>irq3
        sta $ffff
        lda #66+39
        sta $d012

        inc $d019
        rti
}
//multiplex sprites for the scrolltext again
//and call the routine that copies chars
//to the sprites
irq3:
{
        lda #66+42
        sta $d001 + 2*7
        sta $d001 + 2*6
        sta $d001 + 2*5
        sta $d001 + 2*4
        sta $d001 + 2*3
        sta $d001 + 2*2

        ldx #sprite_sidesl/$40 + $2
        ldy #sprite_sidesr/$40 + $2


!:      cmp $d012
        bne !-

        stx screen_pointer+$3f8+4
        sty screen_pointer+$3f8+5

        lda #sprite_letterl/$40 +$2
        sta screen_pointer+$3f8+2
        lda #sprite_letterr/$40 +$2
        sta screen_pointer+$3f8+3

        lda #<irq_enable_bitmap
        sta $fffe
        lda #>irq_enable_bitmap
        sta $ffff
        lda #$7a+1
        sta $d012
        inc $d019
        cli

//one routine does a simple copy (for a d016 = 0)
//the other does a copy with a 4pixels offset (for a d016 = 4)
//if you want slower scroll speed you either need to do more routines (lame)
//or make a routine which can copy the graphics with arbitrary 
//bit offset (cool)       

        lda irq_top_1.s
        bne !+
        //inc $d020
        jsr copy_graphics_to_sprites1
        //dec $d020
        jmp endirq
!:
        //dec $d020
        jsr copy_graphics_to_sprites2
        //inc $d020
endirq:
        inc $d019
        rti
}
//also play music and call the color cycler
//for that color ray line thingy
irq_enable_bitmap:
{
        pha
        stx $e0
        sty $e1
        lda #$10
        sta $d016
        lda #spriteempty/$40
        sta screen_pointer+$3f8+7
        sta screen_pointer+$3f8+6
        sta screen_pointer+$3f8+5
        sta screen_pointer+$3f8+4
        sta screen_pointer+$3f8+3
        sta screen_pointer+$3f8+2
        
        lda #$00
        sta $d015
        
        lda #$3b
        sta $d011
        
        lda #$90
        sta $d018
        lda #$00
        sta $d017

        jsr muzak.play

        lda #$07
        sta $d015
        lda #$06
        sta $d027+0
        sta $d027+1
        sta $d027+2
        lda #$e2
        sta $d001
        sta $d003
        sta $d005
        lda #150
        sta $d000
        lda #150+24
        sta $d002
        lda #150+48
        sta $d004
        ldx #spritelogo/$40+0
        stx screen_pointer + $3f8 + 0
        inx
        stx screen_pointer + $3f8 + 1
        inx
        stx screen_pointer + $3f8 + 2


        jsr cyclecode

        lda #<irq_open_bottom
        sta $fffe
        lda #>irq_open_bottom
        sta $ffff
        lda #249
        sta $d012

        inc $d019
        ldx $e0
        ldy $e1
        pla
        rti
}
//also change the ghost byte overtime
//to show/hide the sprite with credits
irq_open_bottom:
{
        pha
        lda #$33
        sta $d011

        lda #<irq_top_1
        sta $fffe
        lda #>irq_top_1
        sta $ffff
        lda #1
        sta $d012

        lda #$03
        sta $d015
        sta $d01b
        lda #$00
        sta $d017
        sta $d01d
        sta $d01c

        lda #spritecredits/$40
        sta screen_pointer + $3f8+0
        lda #spritecredits/$40+1        
        sta screen_pointer + $3f8+1

        lda skipper:#$00
        and #1
        bne !+
        stx $c0
        ldx ff_index:#$00
        lda ff_tab,x
        sta ghost
        inc ff_index
        ldx $c0
!:      
        inc skipper
        lda ghost:#$00
        sta $7fff
        lda #24
        sta $d000
        lda #48
        sta $d002
        lda #5
        sta $d001
        sta $d003
        pla
        inc $d019
        rti
}
//setup sprites on top and scroll text
.const topx = 115
.const topxx = 140
.const topy = 10
irq_top_1:
{
        lda #$ff
        sta $d015
        sta $d01c
        lda #$00
        sta $d017
        sta $d01b
        sta $7fff

        lda #$40
        sta $d01d

        lda #topy
        sta $d001
        sta $d003
        sta $d005
        sta $d007
        lda #topy+21
        sta $d009
        sta $d00b
        sta $d00d
        sta $d00f

        lda #topx
        sta $d008
        lda #topx+24
        sta $d00a
        lda #topx+48
        sta $d00c
        lda #topx+96
        sta $d00e

        lda #topxx
        sta $d000
        lda #topxx+24
        sta $d002
        lda #topxx+48
        sta $d004
        lda #topxx+72
        sta $d006

        lda #$06
        .for (var i =0;i<8;i++)
        {
            sta $d027+i
        }
        lda #$0e
        sta $d025
        lda #$04
        sta $d026

        ldx #spritestop/$40
        stx screen_pointer + $3f8 + 0
        inx
        stx screen_pointer + $3f8 + 1
        inx
        stx screen_pointer + $3f8 + 2
        inx
        stx screen_pointer + $3f8 + 3
        inx
        stx screen_pointer + $3f8 + 4
        inx
        stx screen_pointer + $3f8 + 5
        stx screen_pointer + $3f8 + 6
        inx
        stx screen_pointer + $3f8 + 7
//setup an irq to change to bitmap mode below the scroller.
//that irq will interrupt this irq because move_chars
//routine takes a lot of rastertime
        lda #<irq
        sta $fffe
        lda #>irq
        sta $ffff
        lda #55
        sta $d012
//enable interrupts again before exiting (rti) this irq       
        inc $d019
        cli
//scroller moves at 4 pixels per frame speed so the $d016
//just toggles between 0 and 4. move_chars is called when $d016=0

        lda s:#$04
        sta irq_go_scroll.scrll//$d016
        beq wrap
        jsr move_chars
        lda s
        sec
        sbc #4
end:    
        sta s

        beq !+
        lda #$10
!:
        inc $d019
        rti
wrap:
        lda #$04
        bne end
}

//these are easy to generate at runtime if you care about size
.align $100
tabhalf1:
.fill $100,(i>>4)
tabhalf2:
.fill $100,(i<<4)
tabof8:
.fill 32, i*8

.import source "font-code.asm"

//just read the charset, AND it with the shape of a sprite
//and write it to the white sprite
copy_graphics_to_sprites1:
{
        .for (var cy = 0; cy < 7; cy++)
        {
            .for (var cx = 0; cx < 3; cx++)
            {
                ldy screen_pointer + 11+cx + (2+cy)*40
                ldx tabof8,y
                .for (var line = 0; line < 8; line++)
                {
                    .var accline = (cy*8+line)
                    .var spr = floor(accline/21)
                    .var lin = mod(accline,21)
                    
                    lda charset+line,x
                    and sprite_sidesl + spr*64 + lin*3 + cx
                    sta sprite_letterl+ spr*64 + lin*3 + cx
                }
            }
            .for (var cx = 0; cx < 3; cx++)
            {
                ldy screen_pointer + 25+cx + (2+cy)*40
                ldx tabof8,y
                .for (var line = 0; line < 8; line++)
                {
                    .var accline = (cy*8+line)
                    .var spr = floor(accline/21)
                    .var lin = mod(accline,21)
                    
                    lda charset+line,x
                    and sprite_sidesr + spr*64 + lin*3 + cx
                    sta sprite_letterr+ spr*64 + lin*3 + cx
                }
            }
        }
        rts
}
//same as above, but we need to shift
//the graphics by 4 pixels. Instead of doing
//4 shifts I just read from a table
//the two AND operations which are commented below
//were commented out because the mask on these bytes
//is always FF, so the AND was useless. If the mask
//was different, these ANDs could be needed.
copy_graphics_to_sprites2:
{
        .for (var cy = 0; cy < 7; cy++)
        {
            .for (var cx = 3; cx >= 0; cx--)
            {
                ldy screen_pointer + 11+cx + (2+cy)*40
                ldx tabof8,y
                .for (var line = 0; line < 8; line++)
                {
                    .var accline = (cy*8+line)
                    .var spr = floor(accline/21)
                    .var lin = mod(accline,21)

                    .if (cx == 3)
                    {
                        ldy charset + line,x
                        lda tabhalf1,y
                        sta zptmp_1 + line
                    }
                    else .if (cx == 2)
                    {
                        ldy charset + line,x
                        lda tabhalf2,y
                        ora tabhalf2
                        //and sprite_sidesl + spr*64 + lin*3 + 2
                        sta sprite_letterl+ spr*64 + lin*3 + 2
                        lda tabhalf1,y
                        sta zptmp_1 +line
                    }
                    else .if (cx == 1)
                    {
                        lda zptmp_1+line
                        ldy charset + line, x
                        ora tabhalf2, y
                        and sprite_sidesl + spr*64 + lin*3 + 1
                        sta sprite_letterl+ spr*64 + lin*3 + 1
                        lda tabhalf1, y
                        sta zptmp_1+line
                    }
                    else
                    {
                        lda zptmp_1+line
                        ldy charset + line, x
                        ora tabhalf2, y
                        and sprite_sidesl + spr*64 + lin*3 + 0
                        sta sprite_letterl+ spr*64 + lin*3 + 0
                    }
                }
            }
            .for (var cx = 3; cx >= 0; cx--)
            {
                ldy screen_pointer + 25+cx + (2+cy)*40
                ldx tabof8,y
                .for (var line = 0; line < 8; line++)
                {
                    .var accline = (cy*8+line)
                    .var spr = floor(accline/21)
                    .var lin = mod(accline,21)

                    .if (cx == 3)
                    {
                        ldy charset + line,x
                        lda tabhalf1,y
                        sta zptmp_1 + line
                    }
                    else .if (cx == 2)
                    {
                        ldy charset + line,x
                        lda tabhalf2,y
                        ora tabhalf2
                        and sprite_sidesr + spr*64 + lin*3 + 2
                        sta sprite_letterr+ spr*64 + lin*3 + 2
                        lda tabhalf1,y
                        sta zptmp_1 +line
                    }
                    else .if (cx == 1)
                    {
                        lda zptmp_1+line
                        ldy charset + line, x
                        ora tabhalf2, y
                        and sprite_sidesr + spr*64 + lin*3 + 1
                        sta sprite_letterr+ spr*64 + lin*3 + 1
                        lda tabhalf1, y
                        sta zptmp_1+line
                    }
                    else
                    {
                        lda zptmp_1+line
                        ldy charset + line, x
                        ora tabhalf2, y
                        //and sprite_sidesr + spr*64 + lin*3 + 0
                        sta sprite_letterr+ spr*64 + lin*3 + 0
                    }
                }
            }
        }
        rts
}

.import source "vic-memory.asm"

* = $7000 "sid"
.fill muzak.size, muzak.getData(i)

	.var gg1 = List().add($09,$02,$08,$0a,$0f,$07,$01,$07,$0f,$0a,$08,$02,$09,$02,$08,$0a)
	.var gg2 = List().add($06,$04,$0e,$05,$03,$0d,$01,$0d,$03,$05,$0e,$04,$06,$06,$04,$0e)

.align $100
colorcycle1:
.fill 128-gg1.size(), $00
.fill gg1.size(), (gg1.get(i))
.fill 64-gg2.size(), $00
.fill gg2.size(), (gg2.get(i))
.fill 64, $00
colorcycle2:
.fill 128-8, $00
.byte $30>>4,$e0>>4,$60>>4,$b0>>4,$00,$00,$00,$00

.fill 32-8, $0
.byte $30>>4,$e0>>4,$60>>4,$b0>>4,$00,$00,$00,$00
.fill 32-8, $0
.byte $30>>4,$e0>>4,$60>>4,$b0>>4,$00,$00,$00,$00

.fill 32-8, $0
.byte $a0>>4,$80>>4,$20>>4,$90>>4,$00,$00,$00,$00
.fill 32-8, $00
.fill 8, $10>>4

.align $100
ff_tab:
.fill 128, $ff
.fill 8, $ff >> i
.fill 64, $00
.fill 8, $1 <<i
.fill (128-(8+64+8)), $ff

//this 'path' file is a 1000 bytes (40x25)
//file with one byte per char. All chars have a byte 0
//but the path of the color line is filled with consecutive values.
//that is forming the path of the line. The loop does 71 tries
//because I know there's only 71 bytes of the path... kinda lame
//way of doing it, but I didn't feel like making a special tool
//to generate that and I had fademaid on hand :P
.var path1 = LoadBinary("gfx/path")
.var cur = 1
.var pointers = List()
.for (var try = 0; try < 71; try++)
{
    .for (var i = 0; i < 1000; i++)
    {
        .if (path1.get(i)==cur)
        {
            .eval pointers.add($d800+i)
            .eval cur++
        }
    }
}
.print ""+pointers.size()
//basically, some bytes of the colorram selected above
//are scrolled from left to right like a scrolltext
//and a new color is pasted at the end. by changing the inc to dec
//and vice versa we can reserve the order of the colors.
cyclecode:
{
        lda skipper:#$00
        and #1
        beq !+
        jmp end
!:
        ldy index:#$00
        .for (var i = 0; i < pointers.size(); i++)
        {
            lda (zpcolorcycleptr),y
            sta pointers.get(i)
            iny
        }
        changedir:inc index
        bne g
        inc zpcolorcycleptr+1

        lda zpcolorcycleptr+1
        cmp #>(colorcycle2+$100)
        bne e
        lda #>colorcycle1
        sta zpcolorcycleptr+1
        
        ldx #DEC_ABS
        lda changedir
        cmp #INC_ABS
        beq !+
        ldx #INC_ABS
!:
        stx changedir
e:
g:
end:
        inc skipper
        rts
}