.var kl = LoadBinary("gfx/bring-down-the-sky_11.kla", BF_KOALA)
* = $4000 "bitmap"
.fill 3*40*8, kl.getBitmap(i)
.align $40
sprite1:
.fill 64, $ff
spritestop:
.import binary "gfx/border_sprites.bin"
//------------------
.var spritesides = LoadPicture("gfx/sprite_sides.bmp")
sprite_sidesl:
.for (var y = 0; y < 3; y++)
{
    .for (var yy =0; yy<21; yy++)
    {
        .byte spritesides.getSinglecolorByte(0, y*21+yy)
        .byte spritesides.getSinglecolorByte(1, y*21+yy)
        .byte spritesides.getSinglecolorByte(2, y*21+yy)
    }
    .byte $00
}
sprite_sidesr:
.for (var y = 0; y < 3; y++)
{
    .for (var yy =0; yy<21; yy++)
    {
        .byte spritesides.getSinglecolorByte(3, y*21+yy)
        .byte spritesides.getSinglecolorByte(4, y*21+yy)
        .byte spritesides.getSinglecolorByte(5, y*21+yy)
    }
    .byte $00
}

spritecredits:
.import binary "gfx/samar_logo_credits_sprites.bin", 3*64
spritelogo:
.import binary "gfx/samar_logo_credits_sprites.bin", 0, 3*64


sprite_letterl:
.fill 3*64,$00
sprite_letterr:
.fill 3*64,$00
* = $4000 + 8*40*8 "rest of the bitmap"
.fill (25-8)*40*8, kl.getBitmap(i + 8*40*8)

* = $6400 "screen"
screen_pointer:
.fill 80, kl.getScreenRam(i)
.fill 7*40, $00
.fill kl.getScreenRamSize()-9*40, kl.getScreenRam(i+9*40)

* = $6800 "scroller charset (8 chars)"
charset:
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$7e,$7e,$7e,$7e,$7e,$7e,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.align $40
spriteempty:
.fill 64, $00
//* = $6840 "sprites"


* = $8000
colorram:
.fill 80, kl.getColorRam(i)
.fill kl.getColorRamSize()-9*40, kl.getColorRam(i+9*40)