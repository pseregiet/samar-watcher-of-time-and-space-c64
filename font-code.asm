.var char_width = List().add(
    1,1,1,1,1,1,1,1,1,5,3,2,3,1,1,1,1,0,1,1,3,1,2,1,2,1,1,6,6,2,5
//  @,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,.,!,?,FAKE
)

move_chars:
{
//copy all chars on the screen to the left
        ldx #00
!:
        .for (var i = 0; i < 7; i++)
        {
            lda screen_pointer + 40*i + 1 + scrollypos*40,x
            sta screen_pointer + 40*i + 0 + scrollypos*40,x
        }
        inx
        cpx #40
        bne !-
//one character is made out of many chars. Load next char
//of the already being drawn character or read a new character
//from the scrolltext
        lda char_of_char:#$08
        cmp #$8
        beq load_new_character
        sta zptmp1
//character | width_index = index to a table with chars
//for example. Letter A (character=1) and with index 3*8 (meaning we
//are drawing 3rd column of chars for a letter A) is 25. 25th byte of
//font_table has a char for that very char, 3rd column of letter A
tryagain:  
        lda character:#$00
        ora zptmp1
        tax

        .for (var i = 0; i < 7; i++)
        {
            lda font_table + i*$100,x
            sta screen_pointer + i*40+39 +scrollypos*40
        }
        inc char_of_char
        rts
load_new_character:
        ldy index:#$00
        lda (zpscrollptr),y
        bne !+
        lda #<text
        sta zpscrollptr
        lda #>text
        sta zpscrollptr+1
        ldy #$00
        sty index
        lda (zpscrollptr),y
!:
        tax
        asl
        asl
        asl
        sta character
        lda widths,x
        sta char_of_char
        sta zptmp1
        iny
        bne !+
        inc zpscrollptr+1
!:      sty index
        jmp tryagain
}

widths:
.fill char_width.size(), char_width.get(i)
//converts ASCII to my encoding. Maybe there's a cleaner
//way of doing it in KickAsm, but it's easy to tell what
//is going on here...

.align $100
text:
.var txt = "             GOING THROUGH TIME AND SPACE.    LOOKING FOR THAT SPECIAL SOMETHING."
.eval txt += "    WHERE WILL THIS JOURNEY TAKE ME TO ?       PASSING THROUGH AND PASSING BY..."
.eval txt += "    @"
.for (var i = 0; i < txt.size(); i++)
{
    .if (txt.charAt(i)=='@') { .byte $00}
    else .if (txt.charAt(i) >= 'A' && txt.charAt(i) <= 'Z')
    {
        .byte ((txt.charAt(i)-'A')+1)
    }
    else .if (txt.charAt(i) == '.')
    {
        .byte 27
    }
    else .if (txt.charAt(i) == '!')
    {
        .byte 28
    }
    else .if (txt.charAt(i) == '?')
    {
        .byte 29
    }
    else .if (txt.charAt(i) == ' ')
    {
        .byte 30
    }
}

//This table stores the screencodes from which
//the characters are made of. This is a blocky font
//so there's only either fully clear char or fully covered

//This font could be more complicated and use 32 characters
//for rounded edges and stuff, but we didn't use it.

.align $100
font_table:
.var fontimg = LoadPicture("gfx/6x7_computer_font_03b.png")//,List().add($000000,$ffffff))
.for (var y = 0; y < 7; y++)
{
    .for (var x = 0; x < 32*8; x++)
    {
        .var b = fontimg.getSinglecolorByte(x,y*8)
        .if (b == $00)
        {
            .byte $00
        }
        else
        {
            .byte $01
        }
    }
}